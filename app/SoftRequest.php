<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoftRequest extends Model
{
    protected $fillable = [
        'title', 'description', 'goal', 'process', 'capability', 'image'
    ];
}
