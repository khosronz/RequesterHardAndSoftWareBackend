<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Resources\v1\UserCollection;
use App\User;
use App\Http\Resources\v1\User as UserResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all(['name', 'email']);

        return new UserCollection($users);
    }


    public function login(Request $request)
    {
        // Validation Data
        $validData = $this->validate($request, [
            'email' => 'required|exists:users',
            'password' => 'required'
        ]);

        // Check Login User
        if (!auth()->attempt($validData)) {
            return response([
                'data' => 'اطلاعات صحیح نیست',
                'status' => 'error'
            ], Response::HTTP_UNAUTHORIZED);
        }

        auth()->user()->update([
            'api_token'=>Str::random(100)
        ]);

        // return response
        return new UserResource(auth()->user());
    }

    public function signup(Request $request)
    {
        // Validation Data
        $validData = $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'confirm' => 'required|string|min:6'
        ]);


        if ($validData['password'] != $validData['confirm']){
            return response([
                'data'=> 'تایید پسورد را درست وارد نکرده اید.',
                'status'=> 'error'
            ],Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user = User::create([
            'name' => $validData['name'],
            'email' => $validData['email'],
            'password' => bcrypt($validData['password']),
            'api_token' => Str::random(100)
        ]);

        return new UserResource($user);
    }

}



