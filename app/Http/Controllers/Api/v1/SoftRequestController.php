<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Resources\v1\SoftRequestCollection;
use App\SoftRequest;
use Illuminate\Http\Request;
use App\Http\Resources\v1\SoftRequest as SoftRequestResource;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class SoftRequestController extends Controller
{
    public function index()
    {
        $softRequests = SoftRequest::all();
//        $softRequests = SoftRequest::paginate(2);
//        return response()->json($softRequests);
//        return SoftRequestResource::collection($softRequests);
        return new SoftRequestCollection($softRequests);
    }

    public function single(SoftRequest $softr)
    {
        return new SoftRequestResource($softr);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response([
                'data' => $validator->errors(),
                'status' => 'error'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return response([
            'data' => [],
            'status' => 'success'
        ], Response::HTTP_OK);
    }
}
