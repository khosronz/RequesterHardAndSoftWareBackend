<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class SoftRequest extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'goal' => $this->goal,
            'process' => $this->process,
            'capability' => $this->capability,
            'image' => $this->image,
            'time' => jdate($this->created_at)->format('datetime'),
        ];
    }
}
