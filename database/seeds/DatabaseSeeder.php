<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user=factory(App\User::class)->create();
        factory(App\SoftRequest::class, 5)->create(['user_id' => $user->id]);
        // $this->call(UsersTableSeeder::class);
    }
}
