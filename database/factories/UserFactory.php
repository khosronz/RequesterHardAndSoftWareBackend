<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'api_token' => \Illuminate\Support\Str::random(100),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\SoftRequest::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(),
        'description' => $faker->paragraph(5),
        'goal' => $faker->paragraph(3),
        'process' => $faker->paragraph(4),
        'capability' => $faker->paragraph(3),
        'image' => 'http://placehold.it/120x120&text=image' . $faker->numberBetween(1, 4)
    ];
});
