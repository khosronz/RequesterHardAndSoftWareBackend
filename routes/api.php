<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->namespace('Api\v1')->group(function (){
    Route::get('/softrequest', 'SoftRequestController@index');
    Route::get('/softrequest/{softrsoftr}', 'SoftRequestController@single');
    Route::post('/softrequest','SoftRequestController@store');

    Route::post('/login', 'UserController@login');
    Route::post('/signup', 'UserController@signup');

    Route::get('/users', 'UserController@index');

    Route::middleware('auth:api')->group(function (){
        Route::get('/user', function (){
            return auth()->user();
        });

        Route::post('/comment', 'CommentController@store');
    });
});

//Route::get('/softrequest', function (){
//    return 'done';
//});

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
